﻿using System.Collections.Generic;

namespace AspNetOpenApiDemo.Model
{
    /// <summary>
    /// Type of beer
    /// </summary>
    public enum BeerStyle
    {
        PaleLager,
        BlondeAle,
        IPA,
        /// <summary>
        /// a double IPA
        /// </summary>
        DoubleIPA,
        /// <summary>
        /// an amber ale
        /// </summary>
        AmberAle,
        DarkLager,
        BrownAle,
        IrishDryStout,
        Stout,
        BalticPorter,
    }

    public class Review
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Rating { get; set; }
    }

    /// <summary>
    /// Description of the Beer class
    /// </summary>
    public class Beer
    {
        /// <summary>
        /// Name of the Beer
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Company marking the beer
        /// </summary>
        public string Company { get; set; }
        public BeerStyle BeerStyle { get; set; }
        public double Alcohol { get; set; }
        public List<Review> Reviews { get; set; }
    }
}
