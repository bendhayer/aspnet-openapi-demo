﻿using System;

namespace AspNetOpenApiDemo
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class OpenApiSetAttribute : Attribute
    {
        public string ApiSet { get;  }

        public OpenApiSetAttribute(string apiSet)
        {
            ApiSet = apiSet;
        }
    }
}
