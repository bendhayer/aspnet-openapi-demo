const path = require('path');
const { merge } = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const config = require('./webpack.config');

module.exports = merge(
  config,
  {
    output: {
      path: path.resolve(__dirname, '..', 'wwwroot'),
      filename: '[name].js',
      publicPath: '/',
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].css',
      }),
    ],
  },
);
