const path = require('path');
const { merge } = require('webpack-merge');
const TerserPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const config = require('./webpack.config');

module.exports = merge(
  config,
  {
    devtool: false,
    output: {
      path: path.resolve(__dirname, '..', 'wwwroot'),
      filename: '[name].[contenthash:8].js',
      publicPath: '/',
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].[contenthash:8].css',
      }),
    ],
    optimization: {
      minimizer: [
        new TerserPlugin({
          parallel: true,
          terserOptions: {
            sourceMap: true,
          },
        }),
        new CssMinimizerPlugin({}),
      ],
    },
  },
);
