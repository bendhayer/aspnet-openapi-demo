{
    "root": true,
    "extends": [
      "airbnb-base",
      "plugin:react/recommended",
      "plugin:@typescript-eslint/recommended"
    ],
    "parser": "@typescript-eslint/parser",
    "plugins": [
      "@babel",
      "@typescript-eslint",
      "jsx-a11y",
      "react",
      "react-hooks"
    ],
    "parserOptions":  {
      "ecmaVersion":  2019,
      "sourceType":  "module"
    },
    "env": {
      "browser": true
    },
    "settings": {
      "react": {
        "version": "detect"
      },
      "import/parsers": {
        "@typescript-eslint/parser": [ ".ts,", ".tsx" ]
      },
      "import/resolver": {
        "node": {
          "extensions": [".js", ".jsx", ".ts", ".tsx"]
        }
      }
    },
    "rules": {
      "curly": ["error"],
      "new-cap": ["error"],
      "eqeqeq": ["error", "smart"],
      "linebreak-style": "off",
      "no-param-reassign": ["error", { "props": false }],
      "no-plusplus" : ["error", {"allowForLoopAfterthoughts": true}],
      "click-events-have-key-events": "off",

      "react-hooks/rules-of-hooks": "error",
      "react-hooks/exhaustive-deps": "warn",

      "react/prop-types": "off",
      "react/jsx-filename-extension": ["error", { "extensions": [".tsx"] }],

      "import/extensions": ["error", "never"],
      "import/no-extraneous-dependencies": ["error"],

      "jsx-a11y/click-events-have-key-events": "off",

      // Disabled rules to prevent conflicts with @typescript-eslint
      "indent": "off",
      "@typescript-eslint/indent": ["error", 2],
      "semi": "off",
      "@typescript-eslint/semi": ["error"],
      "no-shadow": "off",
      "@typescript-eslint/no-shadow": ["error"],
      "no-use-before-define": "off",
      "@typescript-eslint/no-use-before-define": ["error"],
      "lines-between-class-members": "off",
      "@typescript-eslint/lines-between-class-members": ["error"]
    }
  }
