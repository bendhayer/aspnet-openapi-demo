import React, { FC } from 'react';
import {
  Switch,
  NavLink,
  Route,
  Redirect,
} from 'react-router-dom';
import 'rapidoc';

const DocumentationPage: FC<{ url: string; }> = ({ url }) => (
  <div className="page">
    <rapi-doc
      style={{ height: "calc(100vh - 45px)", width: "100%" }}
      spec-url={url}
      theme="light"
      nav-bg-color="#15213a"
      // nav-bg-color = "#154279"
      primary-color ="#1785fb"
      bg-color = "#fff"
      render-styel="read"
      schema-style="tree"
      show-header="false"
      allow-authentication="false"
      allow-server-selection="false"
      allow-try="false"
    >
    </rapi-doc>
  </div>
);

const App: FC = () => (
  <>
    <nav className="navbar">
      <ul className="navbar-links">
        <li><NavLink
          className="navlink"
          activeClassName="active"
          to="/v1.0"
        >
          Version 1.0
        </NavLink></li>
        <li><NavLink
          className="navlink"
          activeClassName="active"
          to="/v1.1"
        >
          Version 1.1
        </NavLink></li>
        <li><NavLink
          className="navlink"
          activeClassName="active"
          to="/v1.1-public"
        >
          Version 1.1 (public)
        </NavLink></li>
      </ul>
    </nav>
    <main className="main">
      <Switch>
        <Route exact path="/">
          <Redirect to="/v1" />
        </Route>
        <Route path="/v1">
          <div className="page">
            <DocumentationPage url="/swagger/v1/swagger.json" />
          </div>
        </Route>
        <Route path="/v1.1">
          <div className="page">
            <DocumentationPage url="/swagger/v1.1/swagger.json" />
          </div>
        </Route>
        <Route path="/v1.1-public">
          <div className="page">
            <DocumentationPage url="/swagger/v1.1-public/swagger.json" />
          </div>
        </Route>
        <Route path="*">
          <Redirect to="/" />
        </Route>
      </Switch>
    </main>
  </>
);

export default App;
