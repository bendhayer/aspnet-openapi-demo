using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders.Physical;
using Microsoft.Extensions.Hosting;
using NSwag.Generation.Processors;
using System;
using System.IO;
using System.Threading.Tasks;

namespace AspNetOpenApiDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
            });
            services
                .AddVersionedApiExplorer(options =>
                {
                    options.GroupNameFormat = "'v'VVV";
                    options.SubstituteApiVersionInUrl = true;
                });

            foreach(var version in new [] { "v1", "v1.1" })
            {
                services.AddOpenApiDocument(options =>
                {
                    options.Title = "OpenApi documentation example";
                    options.Description = "This document is an example of how we can use NSwag, AspNet, and ApiVersioning to document our APIs";
                    options.Version = version;
                    options.ApiGroupNames = new[] { version };
                    options.DocumentName = version;
                });

                services.AddOpenApiDocument(options =>
                {
                    options.OperationProcessors.Insert(0, new IncludeOpenApiSetProcessor("public"));
                    options.Title = "OpenApi documentation example";
                    options.Description = "This document is an example of how we can use NSwag, AspNet, and ApiVersioning to document our APIs";
                    options.Version = version;
                    options.ApiGroupNames = new[] { version };
                    options.DocumentName = version + "-public";
                });
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseOpenApi();
                app.UseSwaggerUi3();
            }

            app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.Use(FallbackMiddlewareHandler);
        }

        private async Task FallbackMiddlewareHandler(HttpContext context, Func<Task> next)
        {
            if (context.Request.Method == "GET")
            {
                var index = new PhysicalFileInfo(new FileInfo("wwwroot/index.html"));
                if (!context.Response.HasStarted)
                {
                    context.Response.ContentType = "text/html";
                    context.Response.StatusCode = 200;
                }
                await context.Response.SendFileAsync(index);
                await context.Response.CompleteAsync();
            }
            else
            {
                context.Response.StatusCode = StatusCodes.Status404NotFound;
                await context.Response.CompleteAsync();
            }
        }
    }
}
