﻿using AspNetOpenApiDemo.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace AspNetOpenApiDemo.Controllers
{
    [ApiVersion("1.1")]
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class BeerController : ControllerBase
    {
        private static readonly List<Beer> Beers = new()
        {
            new Beer
            {
                Name = "Dormunder Export",
                Company = "Dortmunder Union",
                BeerStyle = BeerStyle.PaleLager,
                Alcohol = 5.1,
                Reviews = new List<Review>
                {
                    new Review() {
                        Name = "BeerAdvocate",
                        Description = "Good",
                        Rating = 19,
                    },
                }
            },
            new Beer
            {
                Name = "Dunkel",
                Company = "Erdinger Weißbräu",
                BeerStyle = BeerStyle.DarkLager,
                Alcohol = 5.6,
            },
            new Beer
            {
                Name = "Samuel Adams",
                Company = "Boston Beer Company",
                BeerStyle = BeerStyle.BrownAle,
                Alcohol = 5,
            },
            new Beer
            {
                Name = "Guinness",
                Company = "Guinness Brewery",
                BeerStyle = BeerStyle.IrishDryStout,
                Alcohol = 4.3,
            },
            new Beer
            {
                Name = "Courage Imperial Russian Stout",
                Company = "Courage Brewery",
                BeerStyle = BeerStyle.BalticPorter,
                Alcohol = 10,
            }
        };

        [HttpGet]
        [MapToApiVersion("1.0")]
        public IEnumerable<( string Name, string Company )> Get()
        {
            return Beers.Select(b =>  (b.Name, b.Company));
        }

        [HttpGet]
        [OpenApiSet("public")]
        [MapToApiVersion("1.1")]
        public IEnumerable<Beer> GetV11()
        {
            return Beers;
        }

        public class PostReview
        {
            [Required]
            public string BeerName { get; set; }
            [Required]
            public Review Review { get; set; }
            public DateTime? Date { get; set; }
        }

        /// <summary>
        /// Create new review
        /// </summary>
        /// <param name="review">New review</param>
        /// <returns>Beer with new review</returns>
        [HttpPost("review")]
        public ActionResult<Beer> AddReview(PostReview review)
        {
            var beer = Beers.SingleOrDefault(b => b.Name == review.BeerName);
            if (beer == null)
                return BadRequest("Beer not found");

            if (beer.Reviews == null)
                beer.Reviews = new List<Review>();
            beer.Reviews.Add(review.Review);

            return beer;
        }
    }
}
