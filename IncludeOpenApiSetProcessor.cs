﻿using NSwag.Generation.Processors;
using NSwag.Generation.Processors.Contexts;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetOpenApiDemo
{
    public class IncludeOpenApiSetProcessor : IOperationProcessor
    {
        private string _openApiSet;

        public IncludeOpenApiSetProcessor(string openApiSet)
        {
            _openApiSet = openApiSet;
        }

        public bool Process(OperationProcessorContext context)
        {
            return context.ControllerType.GetCustomAttributes(typeof(OpenApiSetAttribute), true).Any(a => ((OpenApiSetAttribute)a).ApiSet == _openApiSet) ||
                   context.MethodInfo.GetCustomAttributes(typeof(OpenApiSetAttribute), true).Any(a => ((OpenApiSetAttribute)a).ApiSet == _openApiSet);
        }
    }
}
